use async_lock::{RwLock, RwLockWriteGuard};
use futures_util::{future::BoxFuture, FutureExt};
use std::{
    any::{Any, TypeId},
    future::Future,
    hash::{BuildHasher, Hash},
    pin::Pin,
    sync::Arc,
};
use triomphe::Arc as TrioArc;

use super::OptionallyNone;

#[cfg(feature = "future-unstable")]
use super::{Shared, SharedOutput};

const WAITER_MAP_NUM_SEGMENTS: usize = 64;

type ErrorObject = Arc<dyn Any + Send + Sync + 'static>;

pub(crate) enum InitResult<V, E> {
    Initialized(V),
    ReadExisting(V),
    InitErr(Arc<E>),
}

enum WaiterValue<V> {
    Computing,
    Ready(Result<V, ErrorObject>),
    // https://github.com/moka-rs/moka/issues/43
    InitFuturePanicked,
    // https://github.com/moka-rs/moka/issues/59
    EnclosingFutureAborted,
}

type Waiter<V> = TrioArc<RwLock<WaiterValue<V>>>;

// TypeId is the type ID of the concrete error type of generic type E in the
// try_get_with method. We use the type ID as a part of the key to ensure that we can
// always downcast the trait object ErrorObject (in Waiter<V>) into its concrete
// type.
struct WaiterMap<K, V, S>(crate::cht::SegmentedHashMap<(Arc<K>, TypeId), Waiter<V>, S>);

impl<K, V, S> WaiterMap<K, V, S> {
    fn with_hasher(hasher: S) -> Self {
        Self(crate::cht::SegmentedHashMap::with_num_segments_and_hasher(
            WAITER_MAP_NUM_SEGMENTS,
            hasher,
        ))
    }

    fn remove_waiter(&self, cht_key: (Arc<K>, TypeId), hash: u64)
    where
        (Arc<K>, TypeId): Eq + Hash,
        S: BuildHasher,
    {
        self.0.remove(hash, |k| k == &cht_key);
    }

    #[inline]
    fn try_insert_waiter(
        &self,
        cht_key: (Arc<K>, TypeId),
        hash: u64,
        waiter: &Waiter<V>,
    ) -> Option<Waiter<V>>
    where
        (Arc<K>, TypeId): Eq + Hash,
        S: BuildHasher,
    {
        let waiter = TrioArc::clone(waiter);
        self.0.insert_if_not_present(cht_key, hash, waiter)
    }

    #[inline]
    fn cht_key_hash(&self, key: &Arc<K>, type_id: TypeId) -> ((Arc<K>, TypeId), u64)
    where
        (Arc<K>, TypeId): Eq + Hash,
        S: BuildHasher,
    {
        let cht_key = (Arc::clone(key), type_id);
        let hash = self.0.hash(&cht_key);
        (cht_key, hash)
    }
}

struct WaiterGuard<'a, K, V, S>
// NOTE: We usually do not attach trait bounds to here at the struct definition, but
// the Drop trait requires these bounds here.
where
    K: Eq + Hash,
    V: Clone,
    S: BuildHasher,
{
    is_waiter_value_set: bool,
    cht_key: (Arc<K>, TypeId),
    hash: u64,
    waiters: TrioArc<WaiterMap<K, V, S>>,
    write_lock: RwLockWriteGuard<'a, WaiterValue<V>>,
}

impl<'a, K, V, S> WaiterGuard<'a, K, V, S>
where
    K: Eq + Hash,
    V: Clone,
    S: BuildHasher,
{
    fn new(
        cht_key: (Arc<K>, TypeId),
        hash: u64,
        waiters: TrioArc<WaiterMap<K, V, S>>,
        write_lock: RwLockWriteGuard<'a, WaiterValue<V>>,
    ) -> Self {
        Self {
            is_waiter_value_set: false,
            cht_key,
            hash,
            waiters,
            write_lock,
        }
    }

    fn set_waiter_value(&mut self, v: WaiterValue<V>) {
        *self.write_lock = v;
        self.is_waiter_value_set = true;
    }
}

impl<'a, K, V, S> Drop for WaiterGuard<'a, K, V, S>
where
    K: Eq + Hash,
    V: Clone,
    S: BuildHasher,
{
    fn drop(&mut self) {
        if !self.is_waiter_value_set {
            // Value is not set. This means the future containing `*get_with` method
            // has been aborted. Remove our waiter to prevent the issue described in
            // https://github.com/moka-rs/moka/issues/59
            *self.write_lock = WaiterValue::EnclosingFutureAborted;
            self.waiters.remove_waiter(self.cht_key.clone(), self.hash);
            self.is_waiter_value_set = true;
        }
    }
}

#[cfg(feature = "future-unstable")]
pub(crate) type SharedInit<V> =
    Shared<Pin<Box<dyn Future<Output = Result<V, ErrorObject>> + Send + 'static>>>;

#[cfg(feature = "future-unstable")]
pub(crate) fn new_shared_init<O, V, E>(
    init: impl Future<Output = O> + Send + 'static,
    post_init: fn(O) -> Result<V, E>,
) -> SharedInit<V>
where
    O: 'static,
    V: Clone + 'static,
    E: Send + Sync + 'static,
{
    let boxed = async move {
        let o = init.await;
        post_init(o).map_err(|e| Arc::new(e) as ErrorObject)
    }
    .boxed();
    Shared::new(boxed)
}

// TypeId is the type ID of the concrete error type of generic type E in the
// try_get_with method. We use the type ID as a part of the key to ensure that we can
// always downcast the trait object ErrorObject into its concrete type.
#[cfg(feature = "future-unstable")]
struct SharedInitMap<K, V, S>(crate::cht::SegmentedHashMap<(Arc<K>, TypeId), SharedInit<V>, S>);

impl<K, V, S> SharedInitMap<K, V, S> {
    fn with_hasher(hasher: S) -> Self {
        Self(crate::cht::SegmentedHashMap::with_num_segments_and_hasher(
            WAITER_MAP_NUM_SEGMENTS,
            hasher,
        ))
    }

    fn remove_shared_init(&self, cht_key: (Arc<K>, TypeId), hash: u64)
    where
        (Arc<K>, TypeId): Eq + Hash,
        S: BuildHasher,
    {
        self.0.remove(hash, |k| k == &cht_key);
    }

    #[inline]
    fn try_insert_shared_init(
        &self,
        cht_key: (Arc<K>, TypeId),
        hash: u64,
        shared_init: SharedInit<V>,
    ) -> Option<SharedInit<V>>
    where
        (Arc<K>, TypeId): Eq + Hash,
        S: BuildHasher,
    {
        self.0.insert_if_not_present(cht_key, hash, shared_init)
    }

    #[inline]
    fn cht_key_hash(&self, key: &Arc<K>, type_id: TypeId) -> ((Arc<K>, TypeId), u64)
    where
        (Arc<K>, TypeId): Eq + Hash,
        S: BuildHasher,
    {
        let cht_key = (Arc::clone(key), type_id);
        let hash = self.0.hash(&cht_key);
        (cht_key, hash)
    }
}

pub(crate) struct ValueInitializer<K, V, S> {
    waiters: TrioArc<WaiterMap<K, V, S>>,
    #[cfg(feature = "future-unstable")]
    shared_init_map: TrioArc<SharedInitMap<K, V, S>>,
}

impl<K, V, S> ValueInitializer<K, V, S>
where
    K: Eq + Hash + Send + Sync + 'static,
    V: Clone + Send + Sync + 'static,
    S: BuildHasher + Clone + Send + Sync + 'static,
{
    pub(crate) fn with_hasher(hasher: S) -> Self {
        Self {
            #[cfg(feature = "future-unstable")]
            shared_init_map: TrioArc::new(SharedInitMap::with_hasher(hasher.clone())),
            waiters: TrioArc::new(WaiterMap::with_hasher(hasher)),
        }
    }

    //
    // NOTES: We use `Pin<&mut impl Future>` instead of `impl Future` here for the
    // `init` argument. This is because we want to avoid the future size inflation
    // caused by calling nested async functions. See the following links for more
    // details:
    //
    // - https://github.com/moka-rs/moka/issues/212
    // - https://swatinem.de/blog/future-size/
    //

    /// # Panics
    /// Panics if the `init` future has been panicked.
    pub(crate) async fn try_init_or_read<'a, O, E>(
        &'a self,
        key: &Arc<K>,
        type_id: TypeId,
        // Closure to get an existing value from cache.
        mut get: impl FnMut() -> Option<V>,
        // Future to initialize a new value.
        init: Pin<&mut impl Future<Output = O>>,
        // Closure that returns a future to insert a new value into cache.
        mut insert: impl FnMut(V) -> BoxFuture<'a, ()> + Send + 'a,
        // Function to convert a value O, returned from the init future, into
        // Result<V, E>.
        post_init: fn(O) -> Result<V, E>,
    ) -> InitResult<V, E>
    where
        E: Send + Sync + 'static,
    {
        use std::panic::{resume_unwind, AssertUnwindSafe};
        use InitResult::*;

        const MAX_RETRIES: usize = 200;
        let mut retries = 0;

        let (cht_key, hash) = self.waiters.cht_key_hash(key, type_id);

        loop {
            let waiter = TrioArc::new(RwLock::new(WaiterValue::Computing));
            let lock = waiter.write().await;

            match self
                .waiters
                .try_insert_waiter(cht_key.clone(), hash, &waiter)
            {
                None => {
                    // Our waiter was inserted.

                    // Create a guard. This will ensure to remove our waiter when the
                    // enclosing future has been aborted:
                    // https://github.com/moka-rs/moka/issues/59
                    let mut waiter_guard = WaiterGuard::new(
                        cht_key.clone(),
                        hash,
                        TrioArc::clone(&self.waiters),
                        lock,
                    );

                    // Check if the value has already been inserted by other thread.
                    if let Some(value) = get() {
                        // Yes. Set the waiter value, remove our waiter, and return
                        // the existing value.
                        waiter_guard.set_waiter_value(WaiterValue::Ready(Ok(value.clone())));
                        self.waiters.remove_waiter(cht_key, hash);
                        return InitResult::ReadExisting(value);
                    }

                    // The value still does note exist. Let's resolve the init
                    // future. Catching panic is safe here as we do not try to
                    // resolve the future again.
                    match AssertUnwindSafe(init).catch_unwind().await {
                        // Resolved.
                        Ok(value) => {
                            let (waiter_val, init_res) = match post_init(value) {
                                Ok(value) => {
                                    insert(value.clone()).await;
                                    (
                                        WaiterValue::Ready(Ok(value.clone())),
                                        InitResult::Initialized(value),
                                    )
                                }
                                Err(e) => {
                                    let err: ErrorObject = Arc::new(e);
                                    (
                                        WaiterValue::Ready(Err(Arc::clone(&err))),
                                        InitResult::InitErr(err.downcast().unwrap()),
                                    )
                                }
                            };
                            waiter_guard.set_waiter_value(waiter_val);
                            self.waiters.remove_waiter(cht_key, hash);
                            return init_res;
                        }
                        // Panicked.
                        Err(payload) => {
                            waiter_guard.set_waiter_value(WaiterValue::InitFuturePanicked);
                            // Remove the waiter so that others can retry.
                            self.waiters.remove_waiter(cht_key, hash);
                            resume_unwind(payload);
                        }
                    } // The lock will be unlocked here.
                }
                Some(res) => {
                    // Somebody else's waiter already exists. Drop our write lock and
                    // wait for the read lock to become available.
                    std::mem::drop(lock);
                    match &*res.read().await {
                        WaiterValue::Ready(Ok(value)) => return ReadExisting(value.clone()),
                        WaiterValue::Ready(Err(e)) => {
                            return InitErr(Arc::clone(e).downcast().unwrap())
                        }
                        // Somebody else's init future has been panicked.
                        WaiterValue::InitFuturePanicked => {
                            retries += 1;
                            panic_if_retry_exhausted_for_panicking(retries, MAX_RETRIES);
                            // Retry from the beginning.
                            continue;
                        }
                        // Somebody else (a future containing `get_with`/`try_get_with`)
                        // has been aborted.
                        WaiterValue::EnclosingFutureAborted => {
                            retries += 1;
                            panic_if_retry_exhausted_for_aborting(retries, MAX_RETRIES);
                            // Retry from the beginning.
                            continue;
                        }
                        // Unexpected state.
                        WaiterValue::Computing => panic!(
                            "Got unexpected state `Computing` after resolving `init` future. \
                        This might be a bug in Moka"
                        ),
                    }
                }
            }
        }
    }

    #[cfg(feature = "future-unstable")]

    // TODO:
    //
    // 1. [DONE] Currently, all the owners of a shared future will call `insert`
    //    closure and return `Initialized`, which is wrong. Make only one of the
    //    owners to keep doing so, and others to return `ReadExisting`.
    //     - [DONE] To realize this, we will need to copy `Shared` future code from
    //       `futures_util` and modify it.
    // 2. Decide what to do when all the owners of a shared future are dropped
    //    without completing it.
    //     - Do we want to remove the shared future from the waiter map?
    //     - Or, keep it for a configurable timeout and remove it after that?
    // 3. Decide what to do when a shared future is panicked.
    //     - We probably utilize the current `future_utils::future::Shared`
    //       implementation, which causes all the owners of it to panic? (No retrying)
    //         - For now, this behavior is implemented.
    //
    pub(crate) async fn try_init_with_shared_or_read<'a, E>(
        &'a self,
        key: &Arc<K>,
        type_id: TypeId,
        // Closure to get an existing value from cache.
        mut get: impl FnMut() -> Option<V>,
        // Future to initialize a new value.
        shared_init: SharedInit<V>,
        // Closure that returns a future to insert a new value into cache.
        mut insert: impl FnMut(V) -> BoxFuture<'a, ()> + Send + 'a,
    ) -> InitResult<V, E>
    where
        E: Send + Sync + 'static,
    {
        use std::panic::{resume_unwind, AssertUnwindSafe}; // For catch_unwind()

        let (cht_key, hash) = self.shared_init_map.cht_key_hash(key, type_id);
        let inserted_shared_init = self
            .shared_init_map
            .try_insert_shared_init(cht_key.clone(), hash, shared_init.clone())
            .unwrap_or(shared_init);

        // Check if the value has already been inserted by other thread.
        if let Some(value) = get() {
            self.shared_init_map.remove_shared_init(cht_key, hash);
            return InitResult::ReadExisting(value);
        }

        // The value still does note exist. Let's resolve the init
        // future. Catching panic is safe here as we do not try to
        // resolve the future again.
        match AssertUnwindSafe(inserted_shared_init).catch_unwind().await {
            // Resolved
            Ok(v) => match v {
                SharedOutput::JustCompleted(Ok(value)) => {
                    insert(value.clone()).await;
                    self.shared_init_map.remove_shared_init(cht_key, hash);
                    InitResult::Initialized(value)
                }
                SharedOutput::AlreadyCompleted(Ok(value)) => InitResult::ReadExisting(value),
                SharedOutput::JustCompleted(Err(e)) => {
                    self.shared_init_map.remove_shared_init(cht_key, hash);
                    InitResult::InitErr(e.downcast().unwrap())
                }
                SharedOutput::AlreadyCompleted(Err(e)) => {
                    InitResult::InitErr(e.downcast().unwrap())
                }
            },
            // Panicked.
            Err(payload) => {
                if let Some(e) = payload.downcast_ref::<String>() {
                    let panic_msg = if e.starts_with(super::shared::POISONED_LABEL) {
                        // The shared future is poisoned.
                        // TODO: Retry?
                        "shared_init future has been panicked".to_string()
                    } else {
                        // The inner future of the shared future panicked.
                        self.shared_init_map.remove_shared_init(cht_key, hash);
                        e.to_string()
                    };
                    // Propagate the panic.
                    resume_unwind(Box::new(panic_msg));
                } else {
                    // Something unexpected? Propagate the panic anyway.
                    resume_unwind(payload);
                }
            }
        }
    }

    /// The `post_init` function for the `get_with` method of cache.
    pub(crate) fn post_init_for_get_with(value: V) -> Result<V, ()> {
        Ok(value)
    }

    /// The `post_init` function for the `optionally_get_with` method of cache.
    pub(crate) fn post_init_for_optionally_get_with(
        value: Option<V>,
    ) -> Result<V, Arc<OptionallyNone>> {
        // `value` can be either `Some` or `None`. For `None` case, without change
        // the existing API too much, we will need to convert `None` to Arc<E> here.
        // `Infallible` could not be instantiated. So it might be good to use an
        // empty struct to indicate the error type.
        value.ok_or(Arc::new(OptionallyNone))
    }

    /// The `post_init` function for `try_get_with` method of cache.
    pub(crate) fn post_init_for_try_get_with<E>(result: Result<V, E>) -> Result<V, E> {
        result
    }

    /// Returns the `type_id` for `get_with` method of cache.
    pub(crate) fn type_id_for_get_with() -> TypeId {
        // NOTE: We use a regular function here instead of a const fn because TypeId
        // is not stable as a const fn. (as of our MSRV)
        TypeId::of::<()>()
    }

    /// Returns the `type_id` for `optionally_get_with` method of cache.
    pub(crate) fn type_id_for_optionally_get_with() -> TypeId {
        TypeId::of::<OptionallyNone>()
    }

    /// Returns the `type_id` for `try_get_with` method of cache.
    pub(crate) fn type_id_for_try_get_with<E: 'static>() -> TypeId {
        TypeId::of::<E>()
    }
}

fn panic_if_retry_exhausted_for_panicking(retries: usize, max: usize) {
    if retries >= max {
        panic!(
            "Too many retries. Tried to read the return value from the `init` future \
    but failed {} times. Maybe the `init` kept panicking?",
            retries
        );
    }
}

fn panic_if_retry_exhausted_for_aborting(retries: usize, max: usize) {
    if retries >= max {
        panic!(
            "Too many retries. Tried to read the return value from the `init` future \
    but failed {} times. Maybe the future containing `get_with`/`try_get_with` \
    kept being aborted?",
            retries
        );
    }
}
